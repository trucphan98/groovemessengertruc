﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GrooveMessengerDAL.Models.Contact
{
    public class EditContactModel
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }

    }
}
