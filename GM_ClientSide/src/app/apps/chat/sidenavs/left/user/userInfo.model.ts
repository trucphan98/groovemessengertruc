export class UserInfo {
    public id: string;
    public userId: string;
    public displayName: string;
    public mood: string;
    public status: string;
    public avatar: string;
}

